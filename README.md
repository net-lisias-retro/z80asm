z80asm 
=======

This is a **unnoficial repository** maintained by Lisias T. See [here](http://savannah.nongnu.org/projects/z80asm/) for the canonical repository.

For more information, please read:

* [README](README)
* [NEWS](NEWS)
* [ChangeLog](ChangeLog)
* [AUTHORS](AUTHORS)

Disclaimer
----------
Lisias T **is not** the maintainer of the software, just of this repository.

This repository is for source code reference only. **Do not** rely on it for official information or code.

